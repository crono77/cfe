const con = require('./config');
const MongoClient = require('mongodb').MongoClient;
const { OnjectId, ObjectId } = require('mongodb');

const express = require('express');
const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, result) => {
    result.send({ codigo: 200, sms: 'OK-SERVER' });
});

app.route('/clientes')
    .get((req, result) => {

        MongoClient.connect(con.url, function(err, client) {
            if (err) throw err;

            let dbo = client.db(con.dbName);

            dbo.collection('clientes').find({}).toArray((err, res) => {
                if (err) throw err;

                result.send({ codigo: 200, sms: res })
                client.close();
            });

        });

    })
    .post((req, result) => {
        if (!req.query.nombre || !req.query.apellidos) {
            result.send({ codigo: 500, sms: 'Faltan Datos..' });
        } else {

            MongoClient.connect(con.url, function(err, client) {
                if (err) throw err;

                let dbo = client.db(con.dbName);
                let documento = { nombre: req.query.nombre, apellidos: req.query.apellidos };

                dbo.collection("clientes").insertOne(documento, function(err, res) {
                    if (err) throw err;
                    result.send({ codigo: 200, sms: res.ops });
                    client.close();
                });


            });


        }

    })
    .put((req, result) => {
        if (!req.query.nombre || !req.query.apellidos || !req.query.id) {
            result.send({ codigo: 500, sms: 'Faltan Datos..' });
        } else {

            MongoClient.connect(con.url, function(err, client) {
                if (err) throw err;

                let dbo = client.db(con.dbName);

                let objEdit = { '_id': ObjectId(req.query.id) };
                let datos = { $set: { 'nombre': req.query.nombre, 'apellidos': req.query.apellidos } }

                dbo.collection('clientes').updateOne(objEdit, datos, (err, res) => {
                    if (err) throw err;

                    console.log(result);

                    if (result.modifiedCount = 1) {
                        result.send({ codigo: 200, sms: "Elemento Modificado" })
                    } else {
                        result.send({ codigo: 500, sms: "Elemento no Encontrado" })
                    }
                    client.close();
                });


            });

        }


    })
    .delete((req, result) => {
        if (!req.query.id) {
            result.send({ codigo: 500, sms: 'Faltan Datos..' });
        } else {
            MongoClient.connect(con.url, function(err, client) {
                if (err) throw err;

                let dbo = client.db(con.dbName);

                let objElim = { '_id': ObjectId(req.query.id) };

                dbo.collection('clientes').deleteOne(objElim, (err, res) => {
                    if (err) throw err;

                    result.send({ codigo: '200', sms: 'Elemento Eliminado' });
                    client.close();
                });


            });
        }


    })


const server = app.listen(8000, () => {
    console.log('Server en linea....');
});