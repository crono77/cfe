const con = require('./config');
const MongoClient = require('mongodb').MongoClient;


MongoClient.connect(con.url, function(err, client) {
    if (err) throw err;

    let dbo = client.db(con.dbName);
    let documento = { name: "Paco", address: "Rodriguez" };


    dbo.collection("clientes").insertOne(documento, function(err, res) {
        if (err) throw err;
        console.log(res.ops);
        client.close();
    });


});