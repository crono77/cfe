const con = require('./config');
const MongoClient = require('mongodb').MongoClient;


MongoClient.connect(con.url, function(err, client) {
    if (err) throw err;

    let dbo = client.db(con.dbName);

    let objEdit = { 'name': 'Paco' };
    let datos = { $set: { 'name': 'carlos' } }

    dbo.collection('clientes').updateMany(objEdit, datos, (err, res) => {
        if (err) throw err;
        console.log(`Elementos Modificados ${res.upsertedCount}`);
        client.close();
    });


});