const con = require('./config');
const MongoClient = require('mongodb').MongoClient;


MongoClient.connect(con.url, function(err, client) {
    if (err) throw err;

    let dbo = client.db(con.dbName);

    let objElim = { 'name': 'carlos' };

    dbo.collection('clientes').deleteOne(objElim, (err, res) => {
        if (err) throw err;
        console.log(`Elementos eliminados: ${res.deletedCount}`);
        client.close();
    });


});