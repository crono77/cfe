const con = require('./config');
const MongoClient = require('mongodb').MongoClient;


MongoClient.connect(con.url, function(err, client) {
    if (err) throw err;

    let dbo = client.db(con.dbName);

    dbo.collection('clientes').find({ 'name': 'pepe' }).toArray((err, res) => {
        if (err) throw err;
        console.log(res);
        client.close();
    });


});